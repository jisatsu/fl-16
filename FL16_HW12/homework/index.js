function getAge(birthDate) {
  const yearDivider = 31536000000;
  let now = new Date();
  let compare = new Date(now.getTime() - birthDate.getTime());

  return Math.ceil(compare.getTime() / yearDivider);
}

function getWeekDay(date) {
  const weekdays = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];
  return weekdays[date.getDay()];
}

function getAmountDaysToNewYear() {
  const one_day = 86400000;
  const newyear_day = 31;
  const newyear_month = 11;
  let today = new Date();
  let newyear = new Date(today.getFullYear(), newyear_month, newyear_day);
  if (today.getMonth() === newyear_month && today.getDate() > newyear_day) {
    newyear.setFullYear(newyear.getFullYear() + 1);
  }
  return Math.ceil((newyear.getTime() - today.getTime()) / one_day) + 1;
}

function getProgrammersDay(year) {
  const hundred = 100;
  const four = 4;
  const fourHundred = 400;
  const t = 12;
  const tt = 13;

  let isLeap =
    year % hundred === 0 ? year % fourHundred === 0 : year % four === 0;

  let day = isLeap ? tt : t;
  const date = new Date(`September ${day}, ${String(year)} 01:10:00`);
  return `${day} Sep, ${year} (${getWeekDay(date)})`;
}

function howFarIs(weekday) {
  const weekdays = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];
  let target = weekday.charAt(0).toUpperCase() + weekday.slice(1);
  let wd = getWeekDay(new Date());

  if (wd === target) {
    return `Hey, today is ${target} =)`;
  }

  let wdIdx = 0;
  let tarIdx = 0;
  weekdays.forEach((el, idx) => {
    if (el === wd) {
      wdIdx = idx;
    }
    if (el === target) {
      tarIdx = idx;
    }
  });

  let dev = 0;
  const wee = 7;
  if (wdIdx > tarIdx) {
    dev = wee - wdIdx;
  }
  if (wdIdx < tarIdx) {
    dev = tarIdx - wdIdx;
  }

  return `It's ${dev} day(s) left till ${target}`;
}

function isValidIdentifier(ident) {
  const valid = /^[$A-Z_][0-9A-Z_$]*$/i;
  return valid.test(ident);
}

function capitalize(sent) {
  const words = sent.split(' ');

  for (let i = 0; i < words.length; i++) {
    words[i] = words[i][0].toUpperCase() + words[i].substr(1);
  }

  return words.join(' ');
}

function isValidAudioFile(name) {
  const valid = /\.(?:flac|mp3|alac|aac)$/i;
  return valid.test(name);
}

function getHexadecimalColors(str) {
  let result = [];
  const regex = /^#([0-9a-f]{6}|[0-9a-f]{3})$/i;

  let buff = str.split('');
  buff = buff.map((el) => {
    if (el === ';' || el === ',') {
      return ' ';
    }
    return el;
  });
  let newStr = buff.join('');

  let arr = newStr.split(' ');
  for (let i = 0; i < arr.length; i++) {
    if (regex.test(arr[i])) {
      result.push(arr[i]);
    }
  }
  return result;
}

function isValidPassword(pass) {
  const valid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
  return valid.test(pass);
}

function addThousandsSeparators(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function getAllUrlsFromText(text) {
  const regex =
    /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_.~#?&//=]*)/;
  let buff = text.split(' ');
  let result = [];
  buff.forEach((el) => {
    if (regex.test(el)) {
      result.push(el);
    }
  });

  return result;
}
