const appRoot = document.getElementById('app-root');
class State {
  constructor() {
    this.state = {};
  }

  getState() {
    return this.state;
  }

  setState(newState) {
    this.state = { ...this.state, ...newState };
    console.log(this.state);
  }
}
let state = new State();

appRoot.classList.add('wrapper');
renderPage(state);

function renderPage(state) {
  let { regions, languages, countries } = createData(state);
  console.log('RENDER PAGE: ', countries);
  const localState = state.getState();
  if (localState.countrySort || localState.areaSort) {
    countries = sortCountries(countries, localState);
  }
  const tableContent = createTableBody(countries);

  appRoot.innerHTML = `
    <h1 class="header">Countries Search</h1>
    <div style="max-width: 400px; margin: 0 auto;">
    <div class="flex">
        <p>Please choose type of search:</p>    
        <div>
            <div>
                <input 
                  onclick="setSearchType(this);" 
                  value="region" 
                  type="radio" 
                  name="searchType" 
                  id="radioRegion" 
                />
                <label for="radioRegion">By Region</label>
            </div> 
            <div>
                <input 
                  onclick="setSearchType(this);" 
                  value="language" 
                  type="radio" 
                  name="searchType" 
                  id="radioLanguage"
                />
                <label for="radioRegion">By Language</label>
            </div> 
        </div>
    </div>
    <div class="flex">
        <p>Please choose search query:</p>    
        <select name="search-query" id="search-query" ${
          localState.searchType ? '' : 'disabled'
        }>
            <option value="">Select value</option>
            ${localState.searchType === 'region' ? regions : languages}
        </select> 
    </div>
    </div>
    <div style="text-align:center;">${
      localState.searchQuery
        ? `<table id="table" > 
        <th id="countryName">Country Name<span 
        class="arrow"
        onclick="countrySort(this)"
        >${getArrow(localState.countrySort)}</span></th> 
        <th>Capital</th> 
        <th>World Region</th> 
        <th>Languages</th> 
        <th id="areaName">Area <span
        class="arrow"
        onclick="areaSort(this)"
        >${getArrow(localState.areaSort)}</span></th> 
        <th>Flag</th> 
    ${tableContent}
    </table>`
        : 'No items, please choose search query'
    }</div>
    
`;
}

function setSearchType(e) {
  console.log(e.defaultValue);
  state.setState({ searchType: e.defaultValue, searchQuery: undefined });
  renderPage(state);
}

function sortCountries(countries, localState) {
  const minusOne = -1;

  if (localState.areaSort === 'asc') {
    return countries.sort((a, b) => a.area - b.area);
  }
  if (localState.areaSort === 'desc') {
    return countries.sort((a, b) => b.area - a.area);
  }

  if (localState.countrySort === 'asc') {
    return countries.sort(function (a, b) {
      if (a.name < b.name) {
        return minusOne;
      }
      if (b.name < a.name) {
        return 1;
      }
      return 0;
    });
  }
  if (localState.countrySort === 'desc') {
    return countries.sort(function (a, b) {
      if (a.name > b.name) {
        return minusOne;
      }
      if (b.name > a.name) {
        return 1;
      }
      return 0;
    });
  }
}

function getArrow(type) {
  switch (type) {
    case 'asc':
      return '↑';
    case 'desc':
      return '↓';
    default:
      return '⬍';
  }
}

function createData(state) {
  const localState = state.getState();

  const regions = externalService.getRegionsList().reduce((prev, curr) => {
    return (
      prev +
      `<option onclick="setSearchQuery(this);" value="${curr}">${curr}</option>`
    );
  }, '');

  const languages = externalService.getLanguagesList().reduce((prev, curr) => {
    return (
      prev +
      `<option onclick="setSearchQuery(this);" value="${curr}">${curr}</option>`
    );
  }, '');

  let countries;
  //   console.log('QUERY: ', localState.searchQuery);
  if (localState.searchType === 'region') {
    countries = externalService.getCountryListByRegion(localState.searchQuery);
  } else {
    countries = externalService.getCountryListByLanguage(
      localState.searchQuery
    );
  }

  return { regions, languages, countries };
}

function setSearchQuery(e) {
  state.setState({ searchQuery: e.value });
  console.log(e);
  renderPage(state);
}

function createTableBody(value) {
  if (!value) {
    return '';
  }
  let content = '';
  for (let i = 0; i < value.length; i++) {
    content += `<tr class="box row">
        <td>${value[i]['name']} </td>
        <td>${value[i]['capital']} </td>
        <td>${value[i]['region']} </td>
        <td>${getLanguages(value[i]['languages'])}</td>
        <td>${value[i]['area']} </td>
        <td 
            style="background-image: url(${value[i]['flagURL']}); 
            background-repeat: no-repeat; background-position: center;">
        </td>
      </tr>`;
  }
  return content;
}

function getLanguages(value) {
  let result = '';
  for (let i in value) {
    if (Object.prototype.hasOwnProperty.call(value, i)) {
      result = result + value[i] + ', ';
    }
  }
  return result;
}

function areaSort(e) {
  if (e.firstChild.data === '↓') {
    state.setState({ areaSort: 'asc', countrySort: undefined });
  } else if (e.firstChild.data === '↑') {
    state.setState({ areaSort: undefined, countrySort: undefined });
  } else if (e.firstChild.data === '⬍') {
    state.setState({ areaSort: 'desc', countrySort: undefined });
  }
  renderPage(state);
}

function countrySort(e) {
  if (e.firstChild.data === '↓') {
    state.setState({ countrySort: 'asc', areaSort: undefined });
  } else if (e.firstChild.data === '↑') {
    state.setState({ countrySort: undefined, areaSort: undefined });
  } else if (e.firstChild.data === '⬍') {
    state.setState({ countrySort: 'desc', areaSort: undefined });
  }
  renderPage(state);
}
