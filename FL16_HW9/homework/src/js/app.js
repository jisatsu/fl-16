const timeValidation = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/

let eventName = prompt('Please, enter a name of the event') || 'meeting'

let userName = ''
let eventTime = ''
let eventPlace = ''

function showMessage() {
    userName = document.getElementById('name').value
    eventTime = document.getElementById('time').value
    eventPlace = document.getElementById('place').value

    if (!eventTime || !eventName || !eventPlace) {
        alert('Input all data')
    }

    if (!timeValidation.test(eventTime)) {
        alert('Enter time in format hh:mm')
    }

    console.log(
        `${userName} has a ${eventName} today at ${eventTime} somewhere in ${eventPlace} `
    )
}

function convert() {
    const hundred = 100
    const two = 2
    const d = 27.5
    const e = 33.5
    let amountOfEuros = Number(prompt('Input amount of euros'))
    let amountOfDollars = Number(prompt('Input amount of dollars'))

    if (!amountOfDollars || !amountOfEuros) {
        console.log('ERROR: You should input possitive numbers')
    }

    if (amountOfEuros < 0 || amountOfDollars < 0) {
        console.log('ERROR: You should input possitive numbers')
    }

    let dh = amountOfDollars * d
    dh = Math.round(dh * hundred) / hundred.toFixed(two)
    let eh = amountOfEuros * e
    eh = Math.round(eh * hundred) / hundred.toFixed(two)

    alert(
        `${amountOfEuros} euros are equal ${eh} hrns, ${amountOfDollars} dollars are equal ${dh} hrns`
    )
}
