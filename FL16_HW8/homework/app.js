function reverseNumber(num) {
  let sign = 0;
  if (num < 0) {
    sign = -1;
  } else if (num > 0) {
    sign = 1;
  }
  let strArr = String(num).split("");
  let newArr = [];
  for (let i = strArr.length - 1; i >= 0; i--) {
    newArr.push(strArr[i]);
  }

  let str = "";

  for (let i = 0; i < newArr.length; i++) {
    str += String(newArr[i]);
  }
  return parseFloat(str) * sign;
}

function forEach(arr, func) {
  for (let i = 0; i < arr.length; i++) {
    arr[i] = func(arr[i]);
  }
}

function map(arr, func) {
  const mapArr = [];
  forEach(arr, (el) => {
    if (func(el)) {
      mapArr.push(func(el));
    }
  });
  return mapArr;
}

function filter(arr, func) {
  const filterArr = [];
  forEach(arr, (el) => {
    if (func(el)) {
      filterArr.push(el);
    }
  });
  return filterArr;
}

function getAdultAppleLovers(data) {
  let result = filter(data, (el) => {
    return el.age > 18;
  });
  result = map(result, (el) => {
    if (el.favoriteFruit === "apple") {
      return el.name;
    }
  });
  return result;
}

function getKeys(obj) {
  let myKeys = [];
  for (key in obj) {
    myKeys.push(key);
  }
  return myKeys;
}

function getValues(obj) {
  let myValues = [];
  for (key in obj) {
    myValues.push(obj[key]);
  }
  return myValues;
}

function showFormattedDate(dateObj) {
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const date = dateObj.getDate();
  const month = months[dateObj.getMonth()];
  const year = dateObj.getFullYear();
  return `It is ${date} of ${month}, ${year}`;
}
