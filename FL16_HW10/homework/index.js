const isEquals = (a, b) => a === b

const isBigger = (a, b) => a > b

const storeNames = (...names) => names

const getDifference = (a, b) => {
    let x = a - b
    let negativeOne = -1
    return x < 0 ? x * negativeOne : x
}

const negativeCount = (numbers) => {
    let count = 0

    numbers.forEach((num) => {
        if (num < 0) {
            count++
        }
    })

    return count
}

const letterCount = (str, ltr) => {
    let count = 0
    for (let i = 0; i < str.length; i++) {
        if (str[i] === ltr) {
            count++
        }
    }
    return count
}

const countPoints = (arr) => {
    const winCase = 3
    let count = 0
    arr.forEach((score) => {
        let s = score.split(':')

        if (s[0] > s[1]) {
            count += winCase
        } else if (s[0] === s[1]) {
            count++
        }
    })
    return count
}
